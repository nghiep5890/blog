<?php

App::uses('AppController', 'Controller');

/**
 * Blogs Controller
 *
 * @property Blog $Blog
 * @property PaginatorComponent $Paginator
 */
class BlogsController extends AppController {

//    public $actsAs = array('Containable');
    public $components = array('Paginator', 'Flash');

    public function search() {

        if ($this->request->is('post')) {
            $keyword = $this->request->data['Blog']['keyword'];
            $blogs = $this->Blog->find('all', array(
                'fields' => array('title', 'path_image', 'content', 'slug', 'created', 'updated'),
                'contain' => array('User.name', 'Comment.content'),
                'conditions' => array(
                    'published' => 1,
                    'or' => array('title like' => '%' . $keyword . '%',
                        'content like' => '%' . $keyword . '%')
                )
            ));
//            pr($blogs); die();
            if (!empty($blogs)) {
                $this->set('results', $blogs);
            }
        }
    }

    public function index($id = NULL) {
        if ($id) {
            $this->paginate = array(
                'conditions' => array('Blog.category_id' => $id),
                'order' => array('Blog.created' => 'desc'),
                'contain' => array(
                    'User' => array(
                        'fields' => 'name')),
                'limit' => 10
            );
        } else {
            $this->paginate = array(
                'order' => array('Blog.created' => 'desc'),
                'contain' => array(
                    'User' => array(
                        'fields' => 'name')),
                'limit' => 10
            );
        }
        $this->set('blogs', $this->paginate());
    }
    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Blog->exists($id)) {
            throw new NotFoundException(__('Bài viết không tồn tại hoặc đã bị xoá!'));
        }
        $options = array('conditions' => array('Blog.' . $this->Blog->primaryKey => $id));
        $blog = $this->Blog->find('first', $options);
        $this->set('blog', $blog);

        $this->loadModel('Comment');
        $comments = $this->Comment->find('all', array(
            'conditions' => array(
                'blog_id' => $blog['Blog']['id']
            ),
            'order' => array(
                'Comment.created' => 'asc'
            ),
            'contain' => array(
                'User' => array('username')
            )
                )
        );
        $this->set('comments', $comments);
//        Bài viết liên quan
        $related_blogs = $this->Blog->find('all', array(
            'fields' => array('title', 'path_image'),
            'conditions' => array(
                'category_id' => $blog['Blog']['category_id'],
                'Blog.id <>' => $blog['Blog']['id'],
            ),
            'limit' => 5,
            'order' => 'rand()'
        ));
        $this->set('related_blogs', $related_blogs);
        if ($this->Session->check('comment_errors')) {
            $errors = $this->Session->read('comment_erros');
            $this->set('errors', $errors);
            $this->Session->delete('comment_errors');
        }
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $this->loadModel('User');
        $user = $this->User->find('list');
        $this->set('user', $user);
        $this->loadModel('Category');
        $category = $this->Category->find('list');
        $this->set('category', $category);
        if ($this->request->is('post')) {
            $this->Blog->create();
            if ($this->Blog->save($this->request->data)) {
                $this->Flash->success(__('The blog has been saved.'));
                return $this->redirect(array('action' => '/index'));
            } else {
                $this->Flash->error(__('The blog could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Blog->exists($id)) {
            throw new NotFoundException(__('Invalid blog'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Blog->save($this->request->data)) {
                $this->Flash->success(__('The blog has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The blog could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Blog.' . $this->Blog->primaryKey => $id));
            $this->request->data = $this->Blog->find('first', $options);
        }
        $categories = $this->Blog->Category->find('list');
        $users = $this->Blog->User->find('list');
        $this->set(compact('categories', 'users'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Blog->id = $id;
        if (!$this->Blog->exists()) {
            throw new NotFoundException(__('Invalid blog'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Blog->delete()) {
            $this->Flash->success(__('The blog has been deleted.'));
        } else {
            $this->Flash->error(__('The blog could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
