<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

    public $name = 'Users';
    public $helpers = array('Html');
    public $components = array('Paginator', 'Flash', "Auth");

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->User->recursive = 0;
        $this->set('user', $this->Paginator->paginate());
    }

    /**
     * function register
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow("index", "view");
    }

    public function register() {
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('Đăng ký người dùng mới!'));
                return $this->redirect(array('action' => 'login'));
            }
            $this->Session->setFlash(__('Không thể tiếp tục, vui lòng kiểm tra lại!'));
        }
    }

    public function login() {
        $this->layout = FALSE;
        $this->autoRender = FALSE;
        if ($this->request->is('post')) {
            if ($this->Auth->login($this->request->data)) {
                return $this->redirect($this->Auth->redirectUrl("/blogs"));
            }
            $this->Session->setFlash(__('Sai tên đăng nhập - mật khẩu!'));
        }
    }

    public function logout() {
        $this->Auth->logout();
        return $this->redirect(array('controller' => 'blogs', 'action' => 'index'));
    }

    

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('users', $this->User->find('first', $options));
    }


    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return index
     */
    public function edit($id = null) {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return index
     */
    public function delete($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->User->delete()) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
