<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package        app.Controller
 * @link        http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class CommentsController extends Controller {

    function add() {
        if ($this->request->is('post')) {
            $this->Comment->set($this->request->data);
            if ($this->Comment->validates()) {
                $this->Comment->create();
//                pr($this->request->data);
//                die();
                if ($this->Comment->save($this->request->data)) {
                    $this->Flash->success(__('Gửi bình luận thành công!'));
                } else {
                    $this->Flash->error(__('Please, try again!'));
                }
            }
        } else {
            $comment_errors = $this->Comment->validationErrors;
            $this->Session->write('comment_errors', $comment_errors);
        }
        $this->redirect($this->referer());
    }

}
