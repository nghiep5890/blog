<div class="users index">
	<h2><?php echo __('Users'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th>id</th>
			<th>username</th>
	 		<th>email</th>
			<th class="actions"></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($user as $users): ?>
	<tr>
		<td><?php echo h($users['User']['id']); ?>&nbsp;</td>
		<td><?php echo h($users['User']['username']); ?>&nbsp;</td>
		<td><?php echo h($users['User']['email']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $users['User']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $users['User']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $users['User']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $users['User']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
</div>