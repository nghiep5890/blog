<div class="blogs form">
<?php echo $this->Form->create('Blog');  ?>
	<fieldset>
		<legend><?php echo __('Add Blog'); ?></legend>
	<?php
		echo $this->Form->input('title');
		echo $this->Form->input('content',array('class'=>'ckeditor'));
		echo $this->Form->input('path_image',array('type'=>'file'));
		echo $this->Form->input('user_id',array('options'=>$user));
		echo $this->Form->input('category_id',array('options'=>$category));
	?>

	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>