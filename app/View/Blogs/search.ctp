<?php echo $this->Form->create('Blog'); ?>
<?php echo $this->Form->input('keyword', array('label'=>'', 'placeholder'=>'Gõ vào từ khoá cần tìm kiếm...')); ?>
<?php echo $this->Form->end('Search'); ?>
<!--display search-->
<?php if(isset($results)): ?>
    <?php echo $this->element('blogs', array('blogs' => $results)); ?>
<?php else: ?>
    Không tìm thấy bài viết nào!
<?php endif ?>
<!--end-->
