<div class="blogs view">
<h2><?php echo $blog['Category']['name']; ?></h2></br></br>

<h3><?php echo ($blog['Blog']['title']); ?></h3><hr>


                <i class="fa fa-comments"></i><?php echo " ".$blog['Blog']['comment_count']; ?> comments<hr>
		</br></br>
		<?php echo $this->Html->image("/img/blogimg/".$blog['Blog']['path_image'], array('height'=>'300')); ?>
                </br>
                
                
                <p class="content-index">
			<?php echo ($blog['Blog']['content']); ?>
			&nbsp;
                </p>
		
<dl>
		<dt><?php echo __('Category'); ?></dt>
		<dd>
			<?php echo $this->Html->link($blog['Category']['name'], array('controller' => 'blogs', 'action'=>$blog['Category']['slug'], $blog['Category']['name'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo ($blog['Blog']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo ($blog['Blog']['updated']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Người đăng bài'); ?></dt>
		<dd>
			<?php echo $this->Html->link($blog['User']['name'], array('controller' => 'users', 'action' => 'view', $blog['User']['name'])); ?>
			&nbsp;
		</dd>
</dl>
<!-- Hiển thị bình luận -->	
<div class="comment">
    
	<h3><?php echo __('Comments'); ?></h3>
    <?php if (!empty($comments)): ?>	
	<?php foreach ($comments as $comment): ?>
            <?php echo $comment['User']['username']; ?> --
            <?php echo $comment['Comment']['content']; ?> --
            <?php echo $comment['Comment']['created']; ?> </br>
        <?php endforeach ?>
    <?php endif; ?>
</div>
<hr>
<!-- Viết bình luận -->	
</br><div class="comment_form">
    <?php // pr($errors); ?>
    <?php if(isset($errors)): ?>
        <?php foreach ($errors as $error): ?>
            <?php echo $error[0]; ?>
        <?php endforeach; ?>
    <?php endif; ?>    
    <?php echo $this->Form->create('Comment', array('action'=>'add')); ?>
    <fieldset>
        <legend><?php echo __('Viết bình luận'); ?></legend>
    </fieldset>
    <?php
        echo $this->Form->input('user_id',array('label'=>'', 'style' => 'display:none','type'=>'text', 'value'=>1));
        echo $this->Form->input('blog_id',array('label'=>'', 'style' => 'display:none','type'=>'text', 'value'=>$blog['Blog']['id']));
        echo $this->Form->input('content',array('class'=>'nonee','label'=>'Nội Dung Bình Luận','class'=>'ckeditor'));
    ?>
    <?php echo $this->Form->end(__('Gửi')); ?>
</div>
</div>

