
<?php foreach ($blogs as $blog): ?>
<h2><?php echo $this->Html->link($blog['Blog']['title'], array('controller' => 'Blogs', 'action' => 'view', $blog['Blog']['id'])); ?></h2><br/>
<p class="index-img"><?php echo $this->Html->image("/img/blogimg/".$blog['Blog']['path_image'],array('height'=>'200px') ); ?></p>
<p class="index-content"><?php echo $this->Text->truncate(($blog['Blog']['content']),200); ?></p>
<?php echo $this->Html->link('Xem tiếp', array('controller' => 'Blogs', 'action' => 'view', $blog['Blog']['id'])); ?><br/>
Ngày Đăng: <?php echo ($blog['Blog']['created']); ?><br/>
Cập Nhật Mới: <?php echo ($blog['Blog']['updated']); ?><br/>
Người Đăng: <strong><?php echo ($blog['User']['name']); ?></strong><br/>

<br/>
<hr>
<?php endforeach; ?>