<?php if(empty($this->Session->read('Auth.User'))): ?>
<h2><?php echo __('Login'); ?></h2>
<?php
echo $this->Form->create('User',array(
    'url' => array('controller' => 'users', 'action' => 'login'),
    'type'=>'POST'));
echo $this->Form->input('username',array('type'=>'text'));
echo $this->Form->input('password',array('type'=>'password'));
echo $this->Form->end(__('Đăng Nhập')); 
echo $this->Html->link(__('Đăng ký mới!'), array('controller' => 'users','action' => 'register')); 
?>
<?php else: ?>
<h3>Hello Admin</h3><br/>
<?php echo $this->Html->link(__('Quản lí Profile'), array('controller' => 'users','action' => 'view')); ?><br/><br/>
<?php echo $this->Html->link(__('Bài viết đã đăng'), array('controller' => 'blogs','action' => 'list')); ?><br/><br/>
<?php echo $this->Html->link(__('Logout'), array('controller' => 'users','action' => 'logout')); ?>
<?php endif ?>