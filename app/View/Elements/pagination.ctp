<?php
	echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
    echo $this->Paginator->numbers(array('separator' => ''));
    echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
    echo $this->Paginator->counter("Trang {:page}/{:pages}, hiển thị {:current} bài viết trên tổng số {:count} bài.");
?>