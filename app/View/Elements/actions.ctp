<?php if(!empty($this->Session->read('Auth.User'))): ?>
<h3>Controls</h3>
<ul>        
    <li><?php echo $this->Html->link(__('Đăng Bài Mới'), array('controller' => 'blogs','action' => 'add')); ?></li>
    <li><?php echo $this->Html->link(__('List Categories'), array('controller' => 'categories', 'action' => 'index')); ?> </li>
    <li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
    <li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
</ul>
<?php endif; ?>