<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css('style');
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
		echo $this->Html->script('ckeditor/ckeditor');
	?>
</head>
<body>
	<div id="container">
            <div id="header">
                <?php echo $this->element('header'); ?>
            </div>  <!--  endHeader-->
            <div id="content">
                <div id="actionsl">
                    <div id="login">
                        <?php echo $this->element('login'); ?> 
                    </div>
                    <div id="actions">
                        <?php echo $this->element('actions'); ?>    
                    </div>
                </div>
                <div id="bodycontent">
                    <div class="bodycontent">
                    <?php echo $this->fetch('content'); ?>
                    </div>
                </div>
                <div id="listblog">
                    <?php echo $this->element('listblog'); ?>
                </div>
            </div>
            <div id="footer">
            </div>
	</div>
</body>
</html>
