<?php
App::uses('AppModel', 'Model');

/**
 * Blog Model
 *
 * @property Category $Category
 * @property User $User
 * @property Comment $Comment
 */
class Comment extends AppModel
{
    /**
     * Validation rules
     *
     * @var array
     */
    public $actsAs = array('Containable');
    public $validate = array(
        
//        'content' => array(
//            'notBlank' => array(
//                'rule' => array('notBlank'),
//                'message' => 'Chưa nhập nội dung bình luận!',
//                //'allowEmpty' => false,
//                //'required' => false,
//                //'last' => false, // Stop validation after this rule
//                //'on' => 'create', // Limit validation to 'create' or 'update' operations
//            ),
//            'minlength' => array(
//                'rule' => array('minLength',8),
//                'message' => 'Độ dài comment phải lớn hơn 8 kí tự'
//            ),
//        ),
//        
    );

    // The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(

        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),

        'Blog' => array(
            'className' => 'Blog',
            'foreignKey' => 'blog_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => true
        )
    );


}


