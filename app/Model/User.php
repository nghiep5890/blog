<?php

App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Vui lòng nhập vào tên đăng nhập!',
            ),
        ),
        'email' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Vui lòng nhập vào email!',
            ),
        ),
        'password' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Vui lòng nhập vào password!',
            ),
        ),
    );
    public function beforeSave($options = array()) {
        if (!parent::beforeSave($options)) {
            return false;
        }
        if (isset($this->data[$this->alias]['password'])) {
            $hasher = new SimplePasswordHasher();
            $this->data[$this->alias]['password'] = $hasher->hash($this->data[$this->alias]['password']);
        }
        return true;
    }

    public $hasMany = array(
        'Blog' => array(
            'className' => 'Blog',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Comment' => array(
            'className' => 'Comment',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );





};